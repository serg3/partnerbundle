# Partner bundle
Автоматическое создание и удаление партнеров в микросервисе.

## Установка
Добавьте приватный репозиторий в ваш `composer.json`:
```json
{
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@gitlab.com:serg3/partnerbundle.git"
        }
    ]
}
```
Установите бандл:

`composer req cvek/partner-bundle`

## Настройка
Добавьте файл конфигурации:
```yaml
cvek_partner:
    dsn: <your dsn to message broker>
    failure_transport: <failure transport>
    transports:
        partner_created:
            exchange_name: partner
            routing_key: partner_created
            queue: <your queus to catch created events>
        partner_removed:
            exchange_name: partner
            routing_key: partner_removed
            queue: <your queus to catch removed events>
```

Если хотите обрабатывать входящие сообщения самостоятельно, не используя встроенный handler, то укажите параметры `use_builtin_remover` или `use_builtin_creator` в `false`.