<?php

declare(strict_types=1);

namespace Cvek\PartnerBundle\Messenger\Handler;

use Cvek\PartnerBundle\Messenger\Message\PartnerUpdatedMessage;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;

final class PartnerUpdatedHandler implements MessageHandlerInterface
{
    /** @var NormalizerInterface|DenormalizerInterface */
    private SerializerInterface $serializer;
    private ObjectManager $om;
    private string $partnerClass;
    private bool $useHandler;

    public function __construct(?ObjectManager $entityManager,
                                ?ObjectManager $documentManager,
                                SerializerInterface $serializer,
                                string $partnerClass,
                                bool $useHandler)
    {
        $this->om = $entityManager ?? $documentManager;
        $this->partnerClass = $partnerClass;
        $this->useHandler = $useHandler;
        $this->serializer = $serializer;
    }

    public function __invoke(PartnerUpdatedMessage $message)
    {
        if (!$this->useHandler) {
            return;
        }

        $partner = $this->om->find($this->partnerClass, $message->getPartnerId())
            ?? new $this->partnerClass(Uuid::fromString($message->getPartnerId()));

        $this->serializer->denormalize(
            $this->serializer->normalize($message), // convert message to array to be able to use built-in denormalizer
            $this->partnerClass,
            null,
            [AbstractNormalizer::OBJECT_TO_POPULATE => $partner]
        );

        $this->om->persist($partner);
        $this->om->flush();
    }
}
