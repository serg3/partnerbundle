<?php
/*
 * This file is part of the Aqua Delivery package.
 *
 * (c) Sergey Logachev <svlogachev@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Cvek\PartnerBundle\Messenger\Handler;

use Cvek\PartnerBundle\Messenger\Message\PartnerRemovedMessage;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class PartnerRemovedHandler implements MessageHandlerInterface
{
    private ObjectManager $om;
    private string $partnerClass;
    private bool $useHandler;

    public function __construct(?ObjectManager $entityManager,
                                ?ObjectManager $documentManager,
                                string $partnerClass,
                                bool $useHandler)
    {
        $this->om = $entityManager ?? $documentManager;
        $this->partnerClass = $partnerClass;
        $this->useHandler = $useHandler;
    }

    public function __invoke(PartnerRemovedMessage $message)
    {
        if (!$this->useHandler) {
            return;
        }

        if (null === $partner = $this->om->find($this->partnerClass, $message->getPartnerId())) {
            return;
        }

        $this->om->remove($partner);
        $this->om->flush();
    }
}
