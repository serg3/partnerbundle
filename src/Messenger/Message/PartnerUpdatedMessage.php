<?php

declare(strict_types=1);

namespace Cvek\PartnerBundle\Messenger\Message;

use Symfony\Component\Validator\Constraints as Assert;

final class PartnerUpdatedMessage
{
    /**
     * @Assert\NotBlank(message="Partner Id can not be blank")
     * @Assert\Uuid(message="Partner Id must be valid UUID")
     */
    private string $partnerId;

    /**
     * @Assert\NotBlank(message="Currency can not be blank")
     * @Assert\Currency(message="Currency is not valid")
     */
    private string $currency;

    /**
     * @Assert\NotBlank(message="Country can not be blank")
     * @Assert\Country(message="Country is not valid")
     */
    private string $country;

    /**
     * @Assert\NotBlank(message="Language can not be blank")
     * @Assert\Language(message="Language is not valid")
     */
    private string $language;

    private ?string $partnerName = null;
    private ?string $partnerBrandName = null;

    public function __construct(string $partnerId,
                                string $currency,
                                string $country,
                                string $language,
                                ?string $partnerName = null,
                                ?string $partnerBrandName = null)
    {
        $this->partnerId = $partnerId;
        $this->currency = $currency;
        $this->country = $country;
        $this->language = $language;
        $this->partnerName = $partnerName;
        $this->partnerBrandName = $partnerBrandName;
    }

    public function getPartnerId(): string
    {
        return $this->partnerId;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function getPartnerName(): ?string
    {
        return $this->partnerName;
    }

    public function getPartnerBrandName(): ?string
    {
        return $this->partnerBrandName;
    }
}
